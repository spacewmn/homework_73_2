const express = require('express');
const app = express();
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');

app.use(express.json());
app.use(cors());

app.post('/decode', (req, res) => {
    const messages = {...req.body};
    console.log(messages);
    const decode = Vigenere.Decipher(messages.password).crypt(messages.message);
    res.send({decode});
});

app.post('/encode', (req, res) => {
    const messages = {...req.body};
    console.log(messages);
    const encode = Vigenere.Cipher(messages.password).crypt(messages.message)
    res.send({encode});
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});


